#!/bin/sh

MOUNT_DIR=/tmp/recovery
RECOVERY_PART=/dev/disk/by-partlabel/ROOTFS2

mkdir -p "${MOUNT_DIR}"
if ! mount "${RECOVERY_PART}" "${MOUNT_DIR}"; then
    echo "Failed to mount recovery partition"
    exit 1
fi
cp systemctl/* "${MOUNT_DIR}/etc/systemd/system/"
chmod 664 "${MOUNT_DIR}/etc/systemd/system/"{bleemsync.service,ftp.socket,ftp@.service,telnet.socket,telnet@.service}
cp bin/* "${MOUNT_DIR}/usr/bin/"
chmod 755 "${MOUNT_DIR}/usr/bin/"{busybox-bs,reboot-normal,bleemsync_service}
chroot "${MOUNT_DIR}" systemctl enable bleemsync.service ftp.socket telnet.socket
chroot "${MOUNT_DIR}" passwd -d root
umount "${MOUNT_DIR}"
echo "Installation on recovery rootfs done"

mount -o remount,rw /
cp bin_normal/* /usr/bin/
chmod 755 /usr/bin/reboot-recovery
mount -o remount,ro /
echo "Installation on main rootfs done"

echo
echo 'Recovery kit installed. Use `reboot-recovery` on normal system to boot to'
echo 'recovery, and `reboot-normal` on recovery system to boot to normal. Use'
echo 'Use telnet and FTP on recovery as you usually would.'
