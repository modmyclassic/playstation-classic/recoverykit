Recovery Kit for PlayStation Classic
====================================

This kit installs telnet and FTP to the recovery partition of the PlayStation
Classic, along with a method to reboot between recovery and normal systems.

Installation
------------
Dump these files somewhere you can access from the PlayStation Classic, then
`cd` to the directory and run `setup.sh`.

Usage
-----
To reboot to recovery from a normal system, type `reboot-recovery` into a shell.
To reboot to normal from recovery, type `reboot-normal` into a shell. You can
use telnet and FTP as normal while in recovery.

Selecting through Fastboot
--------------------------
If somehow you can't use the reboot commands, you can also switch between systems
by flashing the necessary flags through fastboot. To enter fastboot mode, remove
the bottom cover of your PlayStation Classic. Bridge the two large pads above
the letters "LM-11" on the system board, then connect the USB cable to your
computer. Keep the pads bridged until the bootloader device disconnects from
your computer and the fastboot device connects (usually around 5 seconds).
Then, flash the flag file you want and reboot:

```sh
fastboot flash MISC /path/to/flag/file
fastboot reboot
```

Flag files available:
- `misc_reset.bin`: Set to normal system
- `misc_recovery.bin`: Set to recovery
